<!-- vim: set sw=2 et sts=2 ft=xml: -->
  <preface id="_preface">
    <title>Preface</title>
    <para>This <ulink url="https://www.debian.org/doc/manuals/debian-reference/">Debian Reference (version @-@dr-version@-@)</ulink> (@-@build-date@-@) is intended to provide a broad overview of the Debian system administration as a post-installation user guide.</para>
    <para>The target reader is someone who is willing to learn shell scripts but who is not ready to read all the C sources to figure out how the <ulink url="https://en.wikipedia.org/wiki/GNU">GNU</ulink>/<ulink url="https://en.wikipedia.org/wiki/Linux">Linux</ulink> system works.</para>
    <para>For installation instructions, see:</para>
    <itemizedlist>
      <listitem> <para> <ulink url="https://www.debian.org/releases/stable/installmanual">Debian GNU/Linux Installation Guide for current stable system</ulink> </para> </listitem>
      <listitem> <para> <ulink url="https://www.debian.org/releases/testing/installmanual">Debian GNU/Linux Installation Guide for current testing system</ulink> </para> </listitem>
    </itemizedlist>
    <section id="_disclaimer">
      <title>Disclaimer</title>
      <para>All warranties are disclaimed.  All trademarks are property of their respective trademark owners.</para>
      <para>The Debian system itself is a moving target.  This makes its documentation difficult to be current and correct.  Although the current <literal>testing</literal> version of the Debian system was used as the basis for writing this, some contents may be already outdated by the time you read this.</para>
      <para>Please treat this document as the secondary reference. This document does not replace any authoritative guides. The author and contributors do not take responsibility for consequences of errors, omissions or ambiguity in this document.</para>
    </section>
    <section id="_what_is_debian">
      <title>What is Debian</title>
      <para>The <ulink url="https://www.debian.org">Debian Project</ulink> is an association of individuals who have made common cause to create a free operating system.  It's distribution is characterized by the following.</para>
      <itemizedlist>
        <listitem> <para> Commitment to the software freedom: <ulink url="https://www.debian.org/social_contract">Debian Social Contract and Debian Free Software Guidelines (DFSG)</ulink> </para> </listitem>
        <listitem> <para> Internet based distributed unpaid volunteer effort: <ulink url="https://www.debian.org">https://www.debian.org</ulink> </para> </listitem>
        <listitem> <para> Large number of pre-compiled high quality software packages </para> </listitem>
        <listitem> <para> Focus on stability and security with easy access to the security updates </para> </listitem>
        <listitem> <para> Focus on smooth upgrade to the latest software packages in the <literal>testing</literal> archives </para> </listitem>
        <listitem> <para> Large number of supported hardware architectures </para> </listitem>
      </itemizedlist>
      <para>Free Software pieces in Debian come from
        <ulink url="https://en.wikipedia.org/wiki/GNU">GNU</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Linux">Linux</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Berkeley_Software_Distribution">BSD</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/X_Window_System">X</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Internet_Systems_Consortium">ISC</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Apache_Software_Foundation">Apache</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Ghostscript">Ghostscript</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Common_Unix_Printing_System">Common Unix Printing System </ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Samba_(software)">Samba</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/GNOME">GNOME</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/KDE">KDE</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Mozilla">Mozilla</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/LibreOffice">LibreOffice</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Vim_(text_editor)">Vim</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/TeX">TeX</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/LaTeX">LaTeX</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/DocBook">DocBook</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Perl">Perl</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Python_(programming_language)">Python</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Tcl">Tcl</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Java_(programming_language)">Java</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Ruby_(programming_language)">Ruby</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/PHP">PHP</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Berkeley_DB">Berkeley DB</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/MariaDB">MariaDB</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/PostgreSQL">PostgreSQL</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Sqlite">SQLite</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Exim">Exim</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Postfix_(software)">Postfix</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Mutt_(e-mail_client)">Mutt</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/FreeBSD">FreeBSD</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/OpenBSD">OpenBSD</ulink>,
        <ulink url="https://en.wikipedia.org/wiki/Plan_9_from_Bell_Labs">Plan 9</ulink>
        and many more independent free software projects.
	Debian integrates this diversity of Free Software into one system.
      </para>
    </section>
    <section id="_about_this_document">
      <title>About this document</title>
      <section id="_guiding_rules">
        <title>Guiding rules</title>
        <para>Following guiding rules are followed while compiling this document.</para>
        <itemizedlist>
          <listitem> <para> Provide overview and skip corner cases. (<emphasis role="strong">Big Picture</emphasis>) </para> </listitem>
          <listitem> <para> Keep It Short and Simple. (<emphasis role="strong">KISS</emphasis>) </para> </listitem>
          <listitem> <para> Do not reinvent the wheel. (Use pointers to <emphasis role="strong">the existing references</emphasis>) </para> </listitem>
          <listitem> <para> Focus on non-GUI tools and consoles. (Use <emphasis role="strong">shell examples</emphasis>) </para> </listitem>
          <listitem> <para> Be objective. (Use <ulink url="http://popcon.debian.org/">popcon</ulink> etc.) </para> </listitem>
        </itemizedlist>
        <tip> <para>I tried to elucidate hierarchical aspects and lower levels of the system.</para> </tip>
      </section>
      <section id="_prerequisites">
        <title>Prerequisites</title>
        <warning> <para>You are expected to make good efforts to seek answers by yourself beyond this documentation.  This document only gives efficient starting points.</para> </warning>
        <para>You must seek solution by yourself from primary sources.</para>
        <itemizedlist>
          <listitem> <para> The Debian site at <ulink url="https://www.debian.org">https://www.debian.org</ulink> for the general information </para> </listitem>
          <listitem> <para> The documentation under the "<literal>/usr/share/doc/<emphasis>package_name</emphasis></literal>" directory </para> </listitem>
          <listitem> <para> The Unix style <emphasis role="strong">manpage</emphasis>: "<literal>dpkg -L <emphasis>package_name</emphasis> |grep '/man/man.*/'</literal>" </para> </listitem>
          <listitem> <para> The GNU style <emphasis role="strong">info page</emphasis>: "<literal>dpkg -L <emphasis>package_name</emphasis> |grep '/info/'</literal>" </para> </listitem>
          <listitem> <para> The bug report: <ulink url="https://bugs.debian.org/">http://bugs.debian.org/<emphasis>package_name</emphasis></ulink> </para> </listitem>
          <listitem> <para> The Debian Wiki at <ulink url="https://wiki.debian.org/">https://wiki.debian.org/</ulink> for the moving and specific topics </para> </listitem>
          <listitem> <para> The Single UNIX Specification from the Open Group's The UNIX System Home Page at <ulink url="http://www.unix.org/">http://www.unix.org/</ulink> </para> </listitem>
          <listitem> <para> The free encyclopedia from Wikipedia at <ulink url="https://www.wikipedia.org/">https://www.wikipedia.org/</ulink> </para> </listitem>
          <listitem> <para> <ulink url="https://www.debian.org/doc/manuals/debian-handbook/">The Debian Administrator's Handbook</ulink> </para> </listitem>
          <listitem> <para> The HOWTOs from The Linux Documentation Project (TLDP) at <ulink url="http://tldp.org/">http://tldp.org/</ulink> </para> </listitem>
        </itemizedlist>
        <note> <para>For detailed documentation, you may need to install the corresponding documentation package named with "<literal>-doc</literal>" as its suffix.</para> </note>
      </section>
      <section id="_conventions">
        <title>Conventions</title>
        <para>This document provides information through the following simplified presentation style with <literal>bash</literal>(1) shell command examples.</para>
        <screen># <emphasis>command-in-root-account</emphasis>
$ <emphasis>command-in-user-account</emphasis></screen>
        <para>These shell prompts distinguish account used and correspond to set environment variables as: "<literal>PS1='\$'</literal>" and "<literal>PS2=' '</literal>".  These values are chosen for the sake of readability of this document and are not typical on actual installed system.</para>
        <para>All command examples are run under the English locale "<literal>LANG=en_US.UTF8</literal>". Please don't expect the placeholder strings such as <literal><emphasis>command-in-root-account</emphasis></literal> and <literal><emphasis>command-in-user-account</emphasis></literal> to be translated in command examples.  This is an intentional choice to keep all translated examples to be up-to-date.</para>
        <note> <para>See the meaning of the "<literal>$PS1</literal>" and "<literal>$PS2</literal>" environment variables in <literal>bash</literal>(1).</para> </note>
        <para><emphasis role="strong">Action</emphasis> required by the system administrator is written in the imperative sentence, e.g. "Type Enter-key after typing each command string to the shell."</para>
        <para>The <emphasis role="strong">description</emphasis> column and similar ones in the table may contain a <emphasis role="strong">noun phrase</emphasis> following <ulink url="https://www.debian.org/doc/manuals/developers-reference/best-pkging-practices#bpp-desc-basics">the package short description convention</ulink> which drops leading articles such as "a" and "the".  They may alternatively contain an infinitive phrase as a <emphasis role="strong">noun phrase</emphasis> without leading "to" following the short command description convention in manpages.  These may look funny to some people but are my intentional choices of style to keep this documentation as simple as possible.  These <emphasis role="strong">Noun phrases</emphasis> do not capitalize their starting nor end with periods following these short description convention.</para>
        <note> <para>Proper nouns including command names keeps their case irrespective of their location.</para> </note>
        <para>A <emphasis role="strong">command snippet</emphasis> quoted in a text paragraph is referred by the typewriter font between double quotation marks, such as "<literal>aptitude safe-upgrade</literal>".</para>
        <para>A <emphasis role="strong">text data</emphasis> from a configuration file quoted in a text paragraph is referred by the typewriter font between double quotation marks, such as "<literal>deb-src</literal>".</para>
        <para>A <emphasis role="strong">command</emphasis> is referred by its name in the typewriter font optionally followed by its manpage section number in parenthesis, such as <literal>bash</literal>(1).  You are encouraged to obtain information by typing the following.</para>
        <screen>$ man 1 bash</screen>
        <para>A <emphasis role="strong">manpage</emphasis> is referred by its name in the typewriter font followed by its manpage section number in parenthesis, such as <literal>sources.list</literal>(5).   You are encouraged to obtain information by typing the following.</para>
        <screen>$ man 5 sources.list</screen>
        <para>An <emphasis role="strong">info page</emphasis> is referred by its command snippet in the typewriter font between double quotation marks, such as "<literal>info make</literal>".  You are encouraged to obtain information by typing the following.</para>
        <screen>$ info make</screen>
        <para>A <emphasis role="strong">filename</emphasis> is referred by the typewriter font between double quotation marks, such as "<literal>/etc/passwd</literal>".  For configuration files, you are encouraged to obtain information by typing the following.</para>
        <screen>$ sensible-pager "/etc/passwd"</screen>
        <para>A <emphasis role="strong">directory name</emphasis> is referred by the typewriter font between double quotation marks, such as "<literal>/etc/apt/</literal>".  You are encouraged to explore its contents by typing the following.</para>
        <screen>$ mc "/etc/apt/"</screen>
        <para>A <emphasis role="strong">package name</emphasis> is referred by its name in the typewriter font, such as <literal>vim</literal>.  You are encouraged to obtain information by typing the following.</para>
        <screen>$ dpkg -L vim
$ apt-cache show vim
$ aptitude show vim</screen>
        <para>A <emphasis role="strong">documentation</emphasis> may indicate its location by the filename in the typewriter font between double quotation marks, such as "<literal>/usr/share/doc/base-passwd/users-and-groups.txt.gz</literal>" and "<literal>/usr/share/doc/base-passwd/users-and-groups.html</literal>"; or by its <ulink url="https://en.wikipedia.org/wiki/Uniform_Resource_Locator">URL</ulink>, such as <ulink url="https://www.debian.org">https://www.debian.org</ulink>.  You are encouraged to read the documentation by typing the following.</para>
        <screen>$ zcat "/usr/share/doc/base-passwd/users-and-groups.txt.gz" | sensible-pager
$ sensible-browser "/usr/share/doc/base-passwd/users-and-groups.html"
$ sensible-browser "https://www.debian.org"</screen>
        <para>An <emphasis role="strong">environment variable</emphasis> is referred by its name with leading "<literal>$</literal>" in the typewriter font between double quotation marks, such as "<literal>$TERM</literal>".  You are encouraged to obtain its current value by typing the following.</para>
        <screen>$ echo "$TERM"</screen>
      </section>
      <section id="_the_popcon">
        <title>The popcon</title>
        <para>The <ulink url="http://popcon.debian.org/">popcon</ulink> data is presented as the objective measure for the popularity of each package.  It was downloaded on @-@pop-date@-@ and contains the total submission of @-@pop-submissions@-@ reports over @-@pop-packages@-@ binary packages and @-@pop-architectures@-@ architectures.</para>
        <note> <para>Please note that the <literal>@-@arch@-@</literal> <literal>unstable</literal> archive contains only @-@all-packages@-@ packages currently.  The popcon data contains reports from many old system installations.</para> </note>
        <para>The popcon number preceded with "V:" for "votes" is calculated by "1000 * (the popcon submissions for the package executed recently on the PC)/(the total popcon submissions)".</para>
        <para>The popcon number preceded with "I:"  for "installs" is calculated by "1000 * (the popcon submissions for the package installed on the PC)/(the total popcon submissions)".</para>
        <note> <para>The popcon figures should not be considered as absolute measures of the importance of packages.  There are many factors which can skew statistics.  For example, some system participating popcon may have mounted directories such as "<literal>/bin</literal>" with "<literal>noatime</literal>" option for system performance improvement and effectively disabled "vote" from such system.</para> </note>
      </section>
      <section id="_the_package_size">
        <title>The package size</title>
        <para>The package size data is also presented as the objective measure for each package.  It is based on the "<literal>Installed-Size:</literal>" reported by "<literal>apt-cache show</literal>" or "<literal>aptitude show</literal>" command (currently on <literal>@-@arch@-@</literal> architecture for the <literal>unstable</literal> release).  The reported size is in KiB (<ulink url="https://en.wikipedia.org/wiki/Kibibyte">Kibibyte</ulink> = unit for 1024 bytes).</para>
        <note> <para>A package with a small numerical package size may indicate that the package in the <literal>unstable</literal> release is a dummy package which installs other packages with significant contents by the dependency.  The dummy package enables a smooth transition or split of the package.</para> </note>
        <note> <para>A package size followed by "(*)" indicates that the package in the <literal>unstable</literal> release is missing and the package size for the <literal>experimental</literal> release is used instead.</para> </note>
      </section>
      <section id="_bug_reports_on_this_document">
        <title>Bug reports on this document</title>
        <para>Please file bug reports on the <literal>debian-reference</literal> package using <literal>reportbug</literal>(1) if you find any issues on this document. Please include correction suggestion by "<literal>diff -u</literal>" to the plain text version or to the source.</para>
      </section>
    </section>
    <section id="_reminders_for_new_users">
      <title>Reminders for new users</title>
      <para>Here are some reminders for new users:</para>
      <itemizedlist>
        <listitem> <para> Backup your data </para> </listitem>
        <listitem> <para> Secure your password and security keys </para> </listitem>
        <listitem>
	  <para> <ulink url="https://en.wikipedia.org/wiki/KISS_principle">KISS (keep it simple stupid)</ulink> </para>
          <itemizedlist>
            <listitem> <para> Don't over-engineer your system </para> </listitem>
          </itemizedlist>
        </listitem>
        <listitem>
          <para> Read your log files </para>
          <itemizedlist>
            <listitem> <para> The <emphasis role="strong">FIRST</emphasis> error is the one that counts </para> </listitem>
          </itemizedlist>
        </listitem>
        <listitem> <para> <ulink url="https://en.wikipedia.org/wiki/RTFM">RTFM (read the fine manual)</ulink> </para> </listitem>
        <listitem> <para> Search the Internet before asking questions </para> </listitem>
        <listitem> <para> Don't be root when you don't have to be </para> </listitem>
        <listitem> <para> Don't mess with the package management system </para> </listitem>
        <listitem> <para> Don't type anything you don't understand </para> </listitem>
        <listitem> <para> Don't change the file permissions (before the full security review) </para> </listitem>
        <listitem> <para> Don't leave your root shell until you <emphasis role="strong">TEST</emphasis> your changes </para> </listitem>
        <listitem> <para> Always have an alternative boot media (USB memory stick, CD, …) </para> </listitem>
      </itemizedlist>
    </section>
    <section id="_some_quotes_for_new_users">
      <title>Some quotes for new users</title>
      <para>Here are some interesting quotes from the Debian mailing list which may help enlighten new users.</para>
      <itemizedlist>
        <listitem> <para> "This is Unix.  It gives you enough rope to hang yourself."  --- Miquel van Smoorenburg <literal>&lt;miquels at cistron.nl&gt;</literal> </para> </listitem>
        <listitem> <para> "Unix IS user friendly…  It's just selective about who its friends are." --- Tollef Fog Heen <literal>&lt;tollef at add.no&gt;</literal> </para> </listitem>
      </itemizedlist>
      <para>Wikipedia has article "<ulink url="https://en.wikipedia.org/wiki/Unix_philosophy">Unix philosophy</ulink>" which lists interesting quotes.</para>
    </section>
  </preface>
